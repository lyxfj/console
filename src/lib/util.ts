export function sleep(interval) {
    return new Promise((resolve) => {
        setTimeout(resolve, interval);
    });
}

export interface Check {
    (): Promise<boolean>;
}

export async function checkStatus(check: Check, interval: number = 0) {
    interval = interval === 0 ? 100 : interval;
    while (true) {
        if (await check()) {
            break;
        }
        await sleep(interval);
    }
}