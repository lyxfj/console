import ccServerIn from "./service/cc-server-in";
import ccSocketPush from "./service/cc-socket-push";
import consulTemplate from "./service/consul-template";
import consul from "./service/consul";
import gateway from "./service/gateway";
import kefuNodeServer from "./service/kefu-node-server";
import multichannelAssignJava from "./service/multichannel-assign-java";
import nginx from "./service/nginx";
import ubaServer from "./service/uba-server";
import webimClient from "./service/webim-client";
import webim from "./service/webim";
import wechat from "./service/wechat";

export default {
    "cc-server-in": ccServerIn,
    "cc-socket-push": ccSocketPush,
    "consul-template": consulTemplate,
    "consul": consul,
    "gateway": gateway,
    "kefu-node-server": kefuNodeServer,
    "multichannel-assign-java": multichannelAssignJava,
    "nginx": nginx,
    "uba-server": ubaServer,
    "webim-client": webimClient,
    "webim": webim,
    "wechat": wechat,
};