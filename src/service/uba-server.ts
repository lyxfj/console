import { NodeService } from "../lib/service";

export default class UbaServer extends NodeService {
    public start() {
        return super.start("node", ["app.js"]);
    }
}
