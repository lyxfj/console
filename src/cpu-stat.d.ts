declare module "cpu-stat" {
    namespace cpuStat{
        interface optional {
            sampleMs: number;
            coreIndex: number;
        }
    }
    
    function usagePercent(opts: cpuStat.optional, cb: any): void;
    
    function usagePercent(cb: any): void;
    
    function totalCores(): number;
    
    function clockMHz(coreIndex: number): number;
    
    function avgClockMHz(): number;
}