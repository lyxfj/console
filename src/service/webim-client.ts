import * as fs from "fs";
import * as path from "path";

import log from "../lib/log";
import { NodeService } from "../lib/service";

export default class WebimClient extends NodeService{
    /**
     * 替换webim-client的url
     */
    public start() {
        const webimClient = this.config.service["webim-client"];

        const replaceList = [
            "index.html",
            "index-2.html",
            "view/moor_chat.html",
            "view/chat_demo.html",
            "wapchat.html",
            "javascripts/7moorInit.js",
            "javascripts/7moorSDKInit.js",
        ];

        for (const file of replaceList) {
            const filePath = path.join(webimClient.root, file);
            const content = fs.readFileSync(filePath).toString();
            const replaceContent = content.replace(/webchat\.7moor\.com/g, webimClient.url)
                .replace(/websocket\.7moor\.com/g, webimClient.url)
                .replace(/user-analysis\.7moor\.com/g, webimClient.analysis)
                .replace(/clack\.7moor\.com/g, webimClient.clack);
            fs.writeFileSync(filePath, replaceContent);
            log.app.info(`替换${ filePath }成功`);
        }

        process.exit();
        return super.start();
    }
}
