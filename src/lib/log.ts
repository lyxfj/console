import * as log4js from "log4js";
import serviceSet from "../serviceSet";
import * as path from "path";

const config = require(path.resolve( __dirname, "../../log4js.json"));

for (const serviceName in serviceSet) {
    config.appenders[serviceName] = {
        type: "file",
        maxLogSize: 20480000,
        filename: `log/${ serviceName }.log`,
    };
    config.categories[serviceName] = {
        level: "info",
        appenders: [serviceName],
    };
}

log4js.configure(config);

const log = {
    app: log4js.getLogger("app"),
    request: log4js.getLogger("request"),
    service: log4js.getLogger("service"),
};

for (const serviceName in serviceSet) {
    log[serviceName] = log4js.getLogger(serviceName);
}

export default log;