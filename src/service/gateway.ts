import { NodeService } from "../lib/service";

export default class Gateway extends NodeService {
    public start() {
        return super.start("node", ["app.js"]);
    }
}

