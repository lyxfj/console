import * as fs from "fs";
import * as path from "path";
import { NodeService } from "../lib/service";

export default class CcServerIn extends NodeService {
    public start() {
        return super.start("npm.cmd", ["run", "dev"]);
    }

    public configService() {
        // 配置服务
        const configPath = path.resolve(this.root, "service.json");
        const serviceJson = JSON.parse(fs.readFileSync(configPath).toString());
        serviceJson.service.port = this.port;
        serviceJson.service.version_tag = this.config.version_tag;
        Object.assign(serviceJson.service.remote_versions, this.config.service[this.serviceName].remote_versions);
        fs.writeFileSync(configPath, JSON.stringify(serviceJson, undefined, 4));
    }
}