export interface ServiceConfig {
    ip: string;
    port: number;
    root: string;
    remote_versions?: object;
    filterLog?: string[];
    [propName: string]: any;
}

export interface Config {
    version_tag: string;
    globalFilterLog: string[];
    consulCache?: string;
    service: {
        [serviceName: string]: ServiceConfig;
    },
    autoStart: string[],
}

export interface KvData {
    [key: string]: string;
}

export interface serviceInfo {
    stopTime?: number;
    startTime?: number;
    runningTime?: number;
    memoryUsage?: NodeJS.MemoryUsage;
    status: "stop" | "running" | "start" | "restart";
}

export interface ConsulNodeInfo {
    Node: {
        ID: string;
        Node: string;
        Address: string;
        TaggedAddresses: { [key: string]: string },
        Meta: Object;
        CreateIndex: number;
        ModifyIndex: number;
    };
    Service: {
        ID: string;
        Service: string;
        Tags: string[];
        Address: string;
        Port: 3209;
        EnableTagOverride: boolean;
        CreateIndex: number;
        ModifyIndex: number;
    };
    Checks: {
        Node: string;
        CheckID: string;
        Name: string;
        Status: string;
        Notes: string;
        Output: string;
        ServiceID: string;
        ServiceName: string;
        CreateIndex: number,
        ModifyIndex: number
    }[];
}

export interface OsInfo {
    freemem: number;
    cpu: number;
    timestamp: number;
}

export interface Result {
    success: boolean;
    message?: string;
}
