import * as Redis from "ioredis";

export function createClient(options: Redis.RedisOptions) {
    const redis = new Redis(options);
    return (db: number): Redis.Redis => {
        redis.select(db);
        return redis;
    };
}