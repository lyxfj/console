import * as fs from "fs";
import * as path from "path";
import { JavaService } from "../lib/service";

export default class MultichannelAssignJava extends JavaService {
    public async start() {
        await this.build();
        return super.start("java", ["-jar", "./assign-java.jar"]);
    }

    public configService() {
        // 配置服务
        const configPath = path.resolve(this.root, "service.json");
        const serviceJson = JSON.parse(fs.readFileSync(configPath).toString());
        serviceJson.port = this.port;
        serviceJson.version_tag = this.config.version_tag;
        Object.assign(serviceJson.remote_versions, this.config.service[this.serviceName].remote_versions);
        fs.writeFileSync(configPath, JSON.stringify(serviceJson, undefined, 4));
    }
}