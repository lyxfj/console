/**
 * 编译模板
 * @param {string} template 编译模板
 * @param {object} scope 作用域
 */
export function compile(template: string, scope: any) {
    // with (scope) {
    //     return eval("`" + template.replace(/{{(.+?)}}/g, "`+($1)+`") + "`")
    // }

    // 严格模式下禁止使用with
    template = "`" + template.replace(/{{(.+?)}}/g, "`+($1)+`") + "`";
    let data = "";

    // 搜集变量定义
    for (const key in scope) {
        data += `const ${ key } = ${ JSON.stringify(scope[key]) };\n`;
    }
    return eval(`(function () {
        ${ data }
        return ${ template };
    })()`);
}

export function keyFormat(obj: any) {
    const formatObj: any = {};
    // 变更为驼峰格式
    for (let key in obj) {
        let name = key.replace(/-(\w)/g, function () {
            return arguments[1].toUpperCase();
        });
        formatObj[name] = obj[key];
    }
    return formatObj;
}