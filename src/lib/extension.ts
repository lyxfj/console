import { EventEmitter } from "events";
import { Operation } from "../app";

import { serviceInfo } from "../typing";

export default abstract class Extentsion {
    public serviceEvent: EventEmitter;
    public operation: Operation;
    public services: { [key: string]: serviceInfo };
    

    constructor(serviceEvent: EventEmitter, operation: Operation) {
        this.operation = operation;
        this.serviceEvent = serviceEvent;
        this.services = this.operation.getServices();
    }

    abstract register();
}