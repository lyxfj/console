import * as ip from "ip";
import * as os from "os";
export interface Info {
    ip: string;
    os: string;
    system: string;
    hostname: string;
}
export const info: Info = {
    ip: ip.address(),
    hostname: os.hostname(),
    os: os.type().toLowerCase(),
    system: "",
};

// 系统类型
info.os = os.type().toLowerCase();
if (info.os.search("windows") != -1) {
    info.system = "windows";
}
else if (info.os.search("linux") != -1) {
    info.system = "linux";
}