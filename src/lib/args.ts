import * as path from "path";

// 处理命令行传参
const list = process.argv.splice(2);

export interface Args {
    [key: string]: string;
}

export const args: Args = {};

export { list as argList };

if (list.length % 2 != 0) {
    console.log("args error");
} else {
    for (let i = 0; i < list.length; i += 2) {
        if (!/^-/.test(list[i])) {
            console.log("args error！", `unknow '${ list[i] }', Maybe you wanted to say '-${ list[i] }'`);
            break;
        }
        args[list[i]] = list[i + 1];
    }
    args["-c"] = path.resolve(__dirname, args["-c"]);
}