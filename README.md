# local-consul-setup【node版本】

## 安装

### 环境依赖
* node
* npm

```shell
git clone https://git.7moor.com/yuansong/local-consul-setup_node.git
cd local-consul-setup
npm install
```

## 使用

### 启动

> 关闭当前系统已经启动的consul

```shell
node app.js
```

### 停止
```shell
node stop.js
```

### 文件说明
* `template`
nginx配置的模板文件，脚本会从`config.json`中读取配置写入模板生成nginx的配置

* `kvDataTemplate.json`
consul键值对模板文件，脚本会从`config.json`中读取配置写入模板生成一个新的json，根据此自动配置consul的键值对

* `config.json`
本地项目配置，模板文件都会从中读取数据

* `webim-client-template`
本地部署时使用，包含几个需要替换url的文件，脚本发现`config.json`中配置了`webim-client`时，会替换对应`root`路径下的相关文件。

* `nginx-win/conf/lyx.conf`
模板生成的nginx配置文件，`nginx-win/conf/nginx.conf`导入了该文件

* `consul-template-win/consul-template.template.hcl`
consul-template配置的模板文件，脚本会替换其中的nginx目录

* `consul-template-win/consul-template.hcl`
模板生成的consul-template配置文件

## 说明
当前只支持windows，linux需在`app.js`中添加linux的启动配置←_←