import { NodeService } from "../lib/service";

export default class KefuNodeServer extends NodeService {
    public start() {
        return super.start("node", ["app.js"]);
    }
}
